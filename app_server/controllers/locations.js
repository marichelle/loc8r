// GET 'home' page
module.exports.homeList = function(req, res) {
    res.render('locations-list', {
        title : 'Loc8r: find a place to work with Wi-Fi',
        pageHeader: {
            title: 'Loc8r',
            strapline: 'Find places to work with Wi-Fi near you!'
        },
        sidebar: 'Looking for Wi-Fi and a seat? Loc8r helps you find places to work when out and about. Perhaps with coffee, cake or a pint? Let Loc8r help you find the place you\'re looking for.',
        locations: [{
            name: 'bwe kafe',
            address: '1002 Washington Street, Hoboken, NJ 07030',
            rating: 5,
            facilities: ['Coffee', 'Food', 'Premium Wi-Fi'],
            distance: '0.4 miles'
        },{
            name: 'Ultramarinos',
            address: '260 3rd Street, Hoboken, NJ 07030',
            rating: 4,
            facilities: ['Coffee', 'Food', 'Wi-Fi'],
            distance: '0.5 miles'
        },{
            name: 'Bean Vault Coffee',
            address: '1 Newark Street, Hoboken, NJ 07030',
            rating: 3,
            facilities: ['Coffee', 'Wi-Fi'],
            distance: '0.6 miles'
        }]
    });
}

// GET 'location info' page
module.exports.locationInfo = function(req, res) {
    res.render('location-info', {
        title: 'bwe kafe',
        pageHeader: {title: 'bwe kafe'},
        sidebar: {
            context: 'is on Loc8r because it has accessible Wi-Fi and space to sit down with your laptop and get some work done.',
            callToAction: 'If you\'ve been and you like it - or if you don\'t - please leave a review to help other people just like you.'
        },
        location: {
            name: 'bwe kafe',
            address: '1002 Washington Street, Hoboken, NJ 07030',
            rating: 5,
            facilities: ['Coffee', 'Food', 'Premium Wi-Fi'],
            coordinates: {
                lat: 40.748788,
                lng: -74.027788
            },
            openingTimes: [{
                days: 'Monday - Friday',
                opening: '7:00a',
                closing: '8:00p',
                closed: false
            },{
                days: 'Saturday',
                opening: '8:00a',
                closing: '8:00p',
                closed: false
            },{
                days: 'Sunday',
                opening: '8:00a',
                closing: '8:00p',
                closed: false
            }],
            reviews: [{
                author: 'Maya E.',
                rating: 4,
                timestamp: 'January 17, 2016',
                reviewText: 'Their muffins taste healthy, but the Wi-Fi is fast.'
            }, {
                author: 'Romy N.',
                rating: 5,
                timestamp: 'January 17, 2016',
                reviewText: 'What a great place. I can\'t say enough good things about it.'
            }]
        }
    });
}

// GET 'add review' page
module.exports.addReview = function(req, res) {
    res.render('location-review-form', {
        title : 'Review bwe kafe on Loc8r',
        pageHeader: {title: 'Review bwe kafe'},
        content: 'Loc8r was created to help people find places to sit down and get a bit of work done.\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed lorem ac nisi dignissim accumsan. Nullam sit amet interdum magna. Morbi quis faucibus nisi. Vestibulum mollis ➥ purus quis eros adipiscing tristique. Proin posuere semper tellus, id ➥ placerat augue dapibus ornare. Aenean leo metus, tempus in nisl eget, ➥ accumsan interdum dui. Pellentesque sollicitudin volutpat ullamcorper.'
    });
}